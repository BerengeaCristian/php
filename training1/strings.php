<?php

echo  nl2br("\n");

echo  nl2br("1. Write a PHP script to : \n");
echo  nl2br("a) transform a string all uppercase letters. \n");
echo  nl2br(strtoupper("a) transform a string all uppercase letters. \n"));
echo  nl2br("b) Transform a string all LOWERCASE letters \n");
echo  nl2br(strtolower("b) Transform a string all LOWERCASE letters \n"));
echo  nl2br("c) make a string's first character uppercase. \n");
echo  nl2br(ucfirst("c) make a string's first character uppercase. \n"));
echo  nl2br("d) make a string's first character of all the words uppercase.\n");
echo  nl2br(ucwords("d) make a string's first character of all the words uppercase.\n"));
echo  nl2br("\n");

echo  nl2br(" 2. Write a PHP script to split the following string.\n Sample string : '082307'\n Expected Output : 08:23:07\n");
$array =  str_split( "082307", 2);
echo  implode(":", $array);
echo  nl2br("\n\n");

echo  nl2br(" 3. Write a PHP script to check if a string contains specific string? \n Sample string : 'The quick brown fox jumps over the lazy dog.' \n Check whether the said string contains the string 'jumps'. \n");
echo  strpos('The quick brown fox jumps over the lazy dog.','jumps');
echo  nl2br("\n\n");


echo  nl2br("4. Write a PHP script to convert the value of a PHP variable to string. \n");
$var = 23;
echo strval($var);
echo  nl2br("\n\n");

echo  nl2br(" 5.Write a PHP script to extract the file name from the following string.\nSample String : 'www.example.com/public_html/index.php'\nExpected Output : 'index.php'");
$words = explode('/', 'www.example.com/public_html/index.php');
$lastWord = array_pop($words);
echo  nl2br(" \n $lastWord \n\n");

echo  nl2br(" 6. Write a PHP script to extract the user name from the following email ID. 
 Sample String : 'rayy@example.com' 
  Expected Output : 'rayy' \n");
$array = explode('@','rayy@example.com');
echo  nl2br($array[0]."\n\n");

echo  nl2br("7. Write a PHP script to get the last three characters of a string. 
 Sample String : 'rayy@example.com' 
  Expected Output : 'com'\n");
echo  nl2br(substr("rayy@example.com", -3)."\n\n");

$value1 = 65.45;
$value2 = 104.35;
echo  nl2br("8. Write a PHP script to format values in currency style.
 Sample values : value1 = 65.45, value2 = 104.35
 Expected Result : 169.80\n");
echo  number_format($value1+$value2, 2);



echo  nl2br("9. Write a PHP script to generate simple random password [do not use rand() function] from a given
 string.
  Sample string : '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz'
  Note : Password length may be 6, 7, 8 etc.\n");
echo  nl2br(substr(str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz'),-8)."\n\n");

echo  nl2br("10. Write a PHP script to replace the first 'the' of the following string with 'That'.
Sample date : 'the quick brown fox jumps over the lazy dog.'
Expected Result : That quick brown fox jumps over the lazy dog.\n");
echo  nl2br(str_replace('the','That','the quick brown fox jumps over the lazy dog.')."\n\n");

echo  nl2br("11. Write a PHP script to find first character that is different between two strings.
String1 : 'football'
String2 : 'footboll'".
'Expected Result : First difference between two strings at position 5: "a" vs "o"'."\n");
$String1 = 'football';
$String2 = 'footboll';
$pos = strspn($String1 ^ $String2, "\0");
echo  nl2br($String1[$pos]." ".$String2[$pos]."\n\n");

echo  nl2br('12. Write a PHP script to get the filename component of the following path.
Sample path : "http://www.w3resource.com/index.php"
Expected Output :'."'index'"."\n");
echo  nl2br(explode(".",$lastWord)[0]."\n\n");

echo  nl2br("13. Write a PHP script to print the next character of a specific character.
Sample character : 'a'
Expected Output : 'b'
Sample character : 'z'
Expected Output : 'a'\n");
$c= 'a';
$c++;

echo  nl2br($c. '  '.++$c ."\n\n");

echo  nl2br("\n\n");
echo  nl2br("\n\n");

echo  nl2br("\n\n");
echo  nl2br("\n\n");

echo  nl2br("\n\n");
echo  nl2br("\n\n");

?>