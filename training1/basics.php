<?php

echo nl2br("1 \n");
$color = array('white', 'green', 'red', 'blue', 'black');
echo nl2br("The memory of that scene for me is like a frame of film forever frozen at that moment: the $color[2] carpet,
the $color[1] lawn, the $color[0] house, the leaden sky. The new president and his first lady. - Richard M.
Nixon \n");

echo nl2br("\n 2 \n");
foreach ($color as $value){
    echo $value.',';
}
sort($color);
foreach ($color as $value){
    echo nl2br($value."\n\n");
}

echo nl2br("\n 3 \n");
$ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels",
    "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France" => "Paris", "Slovakia"=>"Bratislava",
    "Slovenia"=>"Ljubljana", "Germany" => "Berlin", "Greece" => "Athens", "Ireland"=>"Dublin",
    "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm",
    "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech
Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga",
    "Malta"=>"Valetta", "Austria" => "Vienna", "Poland"=>"Warsaw") ;

ksort($ceu);

foreach ($ceu as $key => $value){
    echo nl2br("The capital of ".$key." is ".$value."\n");
}

echo nl2br("\n 4 \n");
$x = array(1, 2, 3, 4, 5);
print_r($x);
array_splice($x, 3, -1);
echo nl2br("\n");
print_r($x);

echo nl2br("\n\n  5 \n");
$color = array(4 => 'white', 6 => 'green', 11=> 'red');
echo current($color);

echo nl2br("\n\n  6 \n");
function f($value,$key){
    echo nl2br("$key : $value"."\n");
}
$json = '{"Title": "The Cuckoos Calling",
"Author": "Robert Galbraith",
"Detail":
{ 
"Publisher": "Little Brown"
 }}';
$decoded = json_decode($json,true);
//print_r($decode);
array_walk_recursive($decoded,"f");

echo nl2br("\n\n  7 \n");
$arr = array(1, 2, 3, 4, 5);
array_splice($arr, -2, 0, "$$$");
print_r($arr);

echo nl2br("\n\n  8 \n");
$arr = array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");
var_dump($arr);
ksort($arr);
echo nl2br("\n");
var_dump($arr);
krsort($arr);
echo nl2br("\n");
var_dump($arr);
sort($arr);
echo nl2br("\n");
var_dump($arr);
rsort($arr);
echo nl2br("\n");
var_dump($arr);

function printArray($arr){
    foreach ($arr as $value){
        echo $value." ";
    }
    echo nl2br("\n");
}

echo nl2br("\n\n  9 \n");
$temperatures = array(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74,
62, 62, 65, 64, 68, 73, 75, 79, 73);
sort($temperatures);
$sum=0;
foreach ($temperatures as $value){
    $sum+=$value;
}
echo nl2br($sum/count($temperatures)."\n");
$highest5 = array_slice($temperatures,-5,5);
$lowest5 = array_slice($temperatures,0,5);
printArray($lowest5);
printArray($highest5);

echo nl2br("\n\n  10\n");


echo nl2br("\n\n  11 \n");
$array1 = array(array(77, 87), array(23, 45));
$array2 = array("w3resource", "com");
for($i=0;$i<count($array1);$i++){
    $array1[$i]= array_merge(array($array2[$i]),$array1[$i]);
}
var_dump($array1);


echo nl2br("\n\n  12\n");
$Color = array('A' => 'Blue', 'B' => 'Green', 'c' => 'Red');
function upperOrLowerCaseAll($arr,$upper){
    if($upper == true){
        foreach ($arr as $key => $value){
            $arr[$key]=strtoupper($value);
        }
    }
    else{
        foreach ($arr as $key => $value){
            $arr[$key]=strtolower($value);
        }
    }
    return$arr;
}
$ColorUpper = upperOrLowerCaseAll($Color,true);
var_dump($ColorUpper);
echo nl2br("\n");
$ColorLower = upperOrLowerCaseAll($Color,false);
var_dump($ColorLower);

echo nl2br("\n\n  13\n");
for($i=200;$i<250;$i+=4){
    echo $i.' ';
}

echo nl2br("\n\n  14\n");
$arr = array("abcd","abc","de","hjjj","g","wer");
$shortestLength=strlen($arr[0]);
$longestLength=strlen($arr[0]);
foreach ($arr as $value){
    $length=strlen($value);
    if($length>$longestLength) $longestLength=$length;
    else if($length<$shortestLength) $shortestLength=$length;
}
echo nl2br($shortestLength.'                '.$longestLength."\n");


echo nl2br("\n\n  15\n");
echo random_int(10, 30);


echo nl2br("\n\n  16\n");
$arr = array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");
$logest=0;
foreach (array_keys($arr) as $key){
    $length=strlen($key);
    if($logest<$length) {
        $logest=$length;
        $longestKey=$key;
    }
}
echo  $longestKey;


echo nl2br("\n\n  17\n");
function func($arr){
    $x=0;
    foreach ($arr as $value){
        if($value!=0){
            if($x>$value) $x=$value;
        }
    }
    return $x;
}
echo func(array(0,41,2,8,9));


echo nl2br("\n\n  18\n");
function floorDecimal($number,$precision,$separator){
    $number=number_format($number,$precision);
    return implode($separator,explode(".",strval($number)));
}
echo floorDecimal(12.22222,2,'-');


echo nl2br("\n\n  19\n");
$color = array ( "color" => array ( "a" => "Red", "b" => "Green", "c" => "White"),
    "numbers" => array ( 1, 2, 3, 4, 5, 6 ),
    "holes" => array ( "First", 5 => "Second", "Third"));
echo $color["color"]["a"]."    ".$color["holes"][5];


echo nl2br("\n\n  20\n");
function sortBasedOnPriority($priority,$arr){
    $aux = array_combine ($priority,$arr);
    ksort($aux);
    return array_values($aux);
}
$arr =      array(1,7,3,4,5,6,9,2);
$priority = array(2,5,7,3,4,6,8,1);
$res = sortBasedOnPriority($priority,$arr);

var_dump($res);

?>